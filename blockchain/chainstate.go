package blockchain

import (
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/btcsuite/btcutil/base58"
	badger "github.com/dgraph-io/badger/v2"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
	"gitlab.com/iMil/goxplorer/gendb"
)

type TChainState struct {
	txid    []byte // internal, for reduced storage
	Txid    string `json:"txid"` // exported, for display
	Height  uint64 `json:"height"`
	NSize   uint64 `json:"nsize"`
	Address string `json:"address"`
	Amount  uint64 `json:"amount"`
}

const (
	searchloop = 0
	addrdbloop = 1
	mkdbloop   = 2
	txidloop   = 3
	printloop  = 4
)

var (
	looptype = searchloop
	BDB      *badger.DB
	cCatch   = false
	idx      = uint64(1)
)

// dumpBlockIndex writes block index database to badgerdb
func dumpBlockIndex(batch *badger.WriteBatch) error {
	db, err := gendb.NewDB(gendb.LevelDB, os.Getenv("BTCBLOCKINDEX"))
	if err != nil {
		return err
	}
	defer db.Close()
	fmt.Printf("dumping block index to badger... ")
	lset := make(map[string]string, 1)
	err = db.Iterate(nil, func(k, v []byte) (bool, error) {
		lset[string(k)] = string(v)
		return false, nil
	})
	// for some reason, batch.Set() fails inside the Iterator, it writes 12
	// k: v pairs and no more.
	for k, v := range lset {
		err = batch.Set([]byte(k), []byte(v))
		if err != nil {
			return err
		}
	}
	fmt.Println("done")
	return err
}

// recIndex writes the index where it is when writing
func recIndex(batch *badger.WriteBatch, key []byte) error {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, idx)
	err := batch.Set([]byte("lastkey"), append(b, key...))
	if err != nil {
		return err
	}
	return nil
}

func mkEntry(addr, k, v []byte) TChainState {
	if len(k) < 32 || len(v) < 16 {
		return TChainState{}
	}
	ksize := len(k) - 32
	return TChainState{
		Address: string(k[:ksize]),
		Txid:    hex.EncodeToString(k[ksize:]),
		Height:  binary.LittleEndian.Uint64(v[:8]),
		Amount:  binary.LittleEndian.Uint64(v[8:]),
	}
}

// AddrDB queries an address database created by UtxoDecode by calling
// -ldb chain -addr mkdb:dbpath
func AddrDB(address string) ([]TChainState, error) {
	var ret []TChainState

	p := strings.Split(address, ":")
	if len(p[1]) < 1 {
		return ret, fmt.Errorf("no addresses nor txid given")
	}

	var err error
	if BDB == nil {
		options := badger.DefaultOptions(os.Getenv("BTCADDRDB"))
		options.Logger = nil
		BDB, err = badger.Open(options)
		if err != nil {
			return ret, err
		}
	}
	addr := []byte(p[1])
	if looptype == txidloop {
		if len(addr) < 32 {
			return ret, fmt.Errorf("no valid txid given")
		}
		x, err := hex.DecodeString(p[1])
		if err != nil {
			return ret, err
		}
		return ret, BDB.View(func(txn *badger.Txn) error {
			// find txid
			item, err := txn.Get(x)
			if err != nil {
				return err
			}
			// txid value has the key to addr+txid search
			return item.Value(func(val []byte) error {
				a, err := txn.Get(val)
				if err != nil {
					return err
				}
				k := a.Key()
				return a.Value(func(v []byte) error {
					ret = append(ret, mkEntry(addr, k, v))
					return nil
				})
			}) // txvalue value
		})
	}

	if len(addr) < 25 {
		return ret, fmt.Errorf("no valid address given")
	}

	return ret, BDB.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()
		for it.Seek(addr); it.ValidForPrefix(addr); it.Next() {
			item := it.Item()
			k := item.Key()
			return item.Value(func(v []byte) error {
				ret = append(ret, mkEntry(addr, k, v))
				return nil
			})
		}
		return nil
	})
}

// decompressAmount decompresses the amount of Satoshies in an UTXO
// code taken from
// https://github.com/bitcoin/bitcoin/blob/v0.13.2/src/compressor.cpp#L161#L185
func decompressAmount(x uint64) uint64 {
	// x = 0  OR  x = 1+10*(9*n + d - 1) + e  OR  x = 1+10*(n - 1) + 9
	if x == 0 {
		return 0
	}
	x--
	// x = 10*(9*n + d - 1) + e
	e := int(x % 10)
	x /= 10
	n := uint64(0)
	if e < 9 {
		// x = 9*n + d - 1
		d := uint64((x % 9) + 1)
		x /= 9
		// x = n
		n = x*10 + d
	} else {
		n = x + 1
	}
	for ; e > 0; e-- {
		n *= 10
		e--
	}
	return n
}

// UtxoDecode decodes UTXO values of the chainstate LevelDB database
// This code is an adaptation of:
// https://github.com/in3rsha/bitcoin-utxo-dump/blob/master/utxodump.go
//
// This function has 4 roles:
// - Finding UTXO data corresponding to the first encountered address,
//   this is pretty slow dependending on where is the key located in the
//   chainstate
// - Finding UTXO data corresponding to a TXID passed as a parameter
// - Building a faster database containing addresses + TXID as keys (32 bytes),
//   and height (8 bytes) + amount (8 bytes) as Little Endian values
// - Finding on the address database the UTXO data corresponding to
//   a given address
func UtxoDecode(chainstate string, address string) ([]TChainState, error) {
	var ret []TChainState

	var batch *badger.WriteBatch
	var err error
	var db *leveldb.DB
	lastkey := []byte{}

	// catch ^C
	if !cCatch {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)
		go func() {
			<-c
			fmt.Println("Interrupt signal caught. Shutting down gracefully.")
			if looptype == mkdbloop && batch != nil {
				idx--
				fmt.Printf("Dumping %d elements\n", idx)
				err := recIndex(batch, lastkey) // set new index
				if err != nil {
					fmt.Println(err)
				}
				err = batch.Flush()
				if err != nil {
					fmt.Println(err)
				}
			}
			if BDB != nil {
				fmt.Println("Closing badger db")
				BDB.Close() // close badger DB
			}
			if db != nil {
				fmt.Println("Closing chainstate db")
				db.Close() // close chainstate database
			}
			os.Exit(0)
		}()
		cCatch = true
	}

	// search address in dedicated database
	if strings.HasPrefix(address, "db:") {
		looptype = addrdbloop
		return AddrDB(address)
	}
	// search txid in dedicated database
	if strings.HasPrefix(address, "txid:") {
		looptype = txidloop
		return AddrDB(address)
	}
	if address == "print" {
		looptype = printloop
	}

	// chainstate search prefix, 'C'
	bytesprefix := []byte{0x43}
	// open a write-database to record addresses to create an address database
	if address == "mkdb" {
		looptype = mkdbloop
		if BDB == nil {
			options := badger.DefaultOptions(os.Getenv("BTCADDRDB"))
			options.Logger = nil
			BDB, err = badger.Open(options)
			if err != nil {
				return ret, err
			}
		}

		// find if we already have data in the address database (BDB)
		err = BDB.View(func(txn *badger.Txn) error {
			item, err := txn.Get([]byte("lastkey"))
			if err == badger.ErrKeyNotFound {
				return nil
			}
			if err != nil {
				return err
			}
			// key was found, retrieve value
			return item.Value(func(v []byte) error {
				if err == nil {
					idx = binary.LittleEndian.Uint64(v[:8])
					lastkey = append(lastkey, v...)
					return nil
				}
				return err
			})
		})
		if err != nil && err != badger.ErrKeyNotFound {
			return ret, err
		}
		batch = BDB.NewWriteBatch()
		defer batch.Cancel()

		err = dumpBlockIndex(batch)
		if err != nil {
			return ret, err
		}
	}

	var cs TChainState
	obfKey := make([]byte, 1)

	// open chainstate database
	db, err = leveldb.OpenFile(chainstate, nil)
	if err != nil {
		return ret, err
	}
	defer db.Close()
	// find obfuscation key
	iter := db.NewIterator(util.BytesPrefix([]byte{0xe}), nil)
	for iter.Next() {
		value := iter.Value()
		obfKey = value[1:]
	}
	iter.Release()
	err = iter.Error()
	if err != nil {
		return ret, err
	}
	// iterate over all UTXO (keys starting with 'C' / 0x43 and possibly a txid)
	iter = db.NewIterator(util.BytesPrefix(bytesprefix), nil)
	if len(lastkey) > 0 {
		fmt.Printf("seeking at key %x (index: %d)\n", lastkey[8:], idx)
		iter.Seek(lastkey[8:])
	}
	for iter.Next() {
		key := iter.Key()
		value := iter.Value()
		lastkey = key

		cs.txid = reverse(key[1:33])
		cs.Txid = hex.EncodeToString(cs.txid) // for display

		lval := len(value)

		for k := 0; len(obfKey) < lval; k++ {
			obfKey = append(obfKey, obfKey[k])
		}
		var xor []byte
		for i := range value {
			result := value[i] ^ obfKey[i]
			xor = append(xor, result)
		}

		var off, amount uint64
		cs.Height, off = base128(xor, 0)
		cs.Height = cs.Height >> 1
		amount, off = base128(xor, off)
		cs.Amount = decompressAmount(amount)
		cs.NSize, off = base128(xor, off)

		if cs.NSize > 1 && cs.NSize < 6 {
			off--
		}
		script := xor[off:]
		if cs.NSize == 0 {
			cs.Address = base58.Encode(Hash160ToAddress(script, p2pkhsig))
		}
		if cs.NSize == 1 {
			cs.Address = base58.Encode(Hash160ToAddress(script, p2shsig))
		}
		// P2WPKH
		if cs.NSize == 28 && script[0] == 0x0 && script[1] == 0x14 {
			cs.Address, _ = MkWitAddr(script[2:])
		}
		// P2WSH
		if cs.NSize == 40 && script[0] == 0x0 && script[1] == 0x20 {
			cs.Address, _ = MkWitAddr(script[2:])
		}
		switch looptype {
		// search by txid
		case txidloop:
			ret = append(ret, cs)
		// search by address
		case searchloop:
			if cs.Address == address {
				ret = append(ret, cs)
				continue
			}
		case printloop:
			j, err := json.MarshalIndent(cs, "", "  ")
			if err == nil {
				fmt.Println(string(j))
			}
		case mkdbloop:
			// key+txid: height+amount
			h := make([]byte, 8)
			binary.LittleEndian.PutUint64(h, cs.Height)
			a := make([]byte, 8)
			binary.LittleEndian.PutUint64(a, cs.Amount)
			k := append([]byte(cs.Address), cs.txid...)
			v := append(h, a...)
			err = batch.Set(k, v)
			if err != nil {
				return nil, err
			}
			// record txid: key
			err = batch.Set(cs.txid, k)
			if err != nil {
				return nil, err
			}
			if idx%1000000 == 0 {
				fmt.Printf("records: %d\n", idx)
			}
		}
		idx++
	}
	iter.Release()
	// don't close BDB in HTTP mode
	if os.Getenv("GOXHTTPMODE") != "1" {
		// write batch when on mkdb mode
		if looptype == mkdbloop {
			err = recIndex(batch, lastkey)
			if err != nil {
				return nil, err
			}
			batch.Flush()
		} else {
			BDB.Close()
		}
	}

	return ret, iter.Error()
}
