package blockchain

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"gitlab.com/iMil/goxplorer/gendb"
)

var BlockRecords = []string{
	"nVersion",
	"nHeight",
	"nStatus",
	"nTx",
	"nFile",
	"nDataPos",
	"nUndoPos",
}

var FileRecords = []string{
	"nBlocks",
	"nSize",
	"nUndoSize",
	"nHeightFirst",
	"nHeightLast",
	"nTimeFirst",
	"nTimeLast",
}

type TBlockRecords struct {
	NVersion uint64 `json:"nversion"`
	NHeight  uint64 `json:"nheight"`
	NStatus  uint64 `json:"nstatus"`
	NTx      uint64 `json:"ntx"`
	NFile    uint64 `json:"nfile"`
	NDataPos uint64 `json:"ndatapos"`
	NUndoPos uint64 `json:"nundopos"`
}

type TFileRecords struct {
	NBlocks      uint64 `json:"nblocks"`
	NSize        uint64 `json:"nsize"`
	NUndoSize    uint64 `json:"nundosize"`
	NHeightFirst uint64 `json:"nheightfirst"`
	NHeightLast  uint64 `json:"nheightlast"`
	NTimeFirst   uint64 `json:"ntimefirst"`
	NTimeLast    uint64 `json:"ntimeLast"`
}

var allBlocks = []TBlockRecords{}

// base128 returns the field starting at `offset`.
// binary.Uvarint is supposed to do this, except it doesn't.
// And it is not the same varint as in bitcoin serialized varints.
// this is ported from "The Undocumented Internals of The Bitcoin
// and Ethereum Blockchains" python code.
// Definition https://en.wikipedia.org/wiki/LEB128
func base128(b []byte, offset uint64) (uint64, uint64) {
	for n := 0; ; n++ {
		ch := int(b[offset])
		offset++
		n = (n << 7) | (ch & 0x7f)
		if ch&0x80 != 128 {
			return uint64(n), offset
		}
	}
}

// ReadRecord  finds record `r` in `data` LevelDB value
func ReadRecord(data []byte, records []string, r string) (uint64, error) {
	var t, off uint64
	for _, k := range records {
		t, off = base128(data, off)

		if k == r {
			return t, nil
		}
	}
	return 0, fmt.Errorf("no such record")
}

// GetKeyVal returns the value for a given key
func GetKeyVal(db *gendb.DB, k string, param string) ([]byte, error) {
	if k == "f" {
		i, err := strconv.Atoi(param)
		if err != nil {
			return nil, err
		}
		param = fmt.Sprintf("%08x", i)
	}
	x, err := hex.DecodeString(param)
	if err != nil {
		return nil, err
	}
	// 'b' + reversed 32-byte block hash -> block index record
	v := append([]byte(k), reverse(x)...)
	data, err := db.Get(v)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// GetBlockRecordByHeight returns the block record corresponding to a database
// and block height given as parameters
func GetBlockRecordByHeight(db *gendb.DB, h uint64) ([]byte, error) {
	var data []byte
	var err error
	err = db.Iterate([]byte{0x62}, func(k, v []byte) (bool, error) {
		r, err := ReadRecord(v, BlockRecords, "nHeight")
		if err != nil {
			return false, err
		}
		if r == h {
			data = append(data, v...)
			return true, nil // breat iteration loop
		}
		return false, nil
	})
	if data == nil {
		err = fmt.Errorf("no matching data in database")
	}
	if err != nil {
		return nil, err
	}
	return data, nil
}

// GetFileRecordByDate returns a blockfile number corresponding to a given date
func GetFileRecordByDate(db *gendb.DB, t string) (uint64, error) {
	layout := "2006-01-02@15:04:05"
	tlen := len(t)
	ft := t // full time format
	if tlen < 19 {
		if tlen == 10 { // 2006/01/02
			ft = t + "@00:00:00"
		} else {
			return 0, fmt.Errorf("wrong date format")
		}
	}

	ts, err := time.Parse(layout, ft)
	if err != nil {
		return 0, err
	}
	tu := uint64(ts.Unix())

	var blk uint32
	err = db.Iterate(nil, func(k, v []byte) (bool, error) {
		t1, err := ReadRecord(v, FileRecords, "nTimeFirst")
		if err != nil {
			return false, err
		}
		t2, err := ReadRecord(v, FileRecords, "nTimeLast")
		if err != nil {
			return false, err
		}
		if tu >= t1 && tu <= t2 {
			blk = binary.LittleEndian.Uint32(k[1:])
			return true, nil // break
		}
		return false, nil // continue
	})
	if err != nil {
		return 0, err
	}

	return uint64(blk), nil
}

// GetLDBBlock fills TBlockRecords struct with values got with a block key (`b`)
func GetLDBBlock(db *gendb.DB, hash string) (TBlockRecords, error) {
	data, err := GetKeyVal(db, "b", hash)
	if err != nil {
		return TBlockRecords{}, err
	}

	var s TBlockRecords
	v := reflect.ValueOf(s)
	for i, br := range BlockRecords {
		r, err := ReadRecord(data, BlockRecords, br)
		if err != nil {
			return TBlockRecords{}, err
		}
		fname := v.Type().Field(i).Name
		f := reflect.ValueOf(&s).Elem().FieldByName(fname)
		if v.IsValid() {
			f.SetUint(r)
		}
	}
	return s, nil
}

// GetLDBFile fills TFileRecords struct with values got with a file key (`f`)
func GetLDBFile(db *gendb.DB, file string) (TFileRecords, error) {
	data, err := GetKeyVal(db, "f", file)
	if err != nil {
		return TFileRecords{}, err
	}

	var s TFileRecords
	v := reflect.ValueOf(s)
	for i, fr := range FileRecords {
		r, err := ReadRecord(data, FileRecords, fr)
		if err != nil {
			return TFileRecords{}, err
		}
		fname := v.Type().Field(i).Name
		f := reflect.ValueOf(&s).Elem().FieldByName(fname)
		if v.IsValid() {
			f.SetUint(r)
		}
	}
	return s, nil
}
