package blockchain

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"testing"
)

const (
	addressNoMatch = "address does not match"
)

var (
	testblkfile             = fmt.Sprintf("../%s/blk01845.dat", os.Getenv("BTCBLOCKSHOME"))
	blk1845Mkroot    string = "e94cc62a830ab89ae209b4270b55adea14446de454bbbaf94a96e2f71d3c419f"
	blk1845blkhash   string = "000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2"
	swTransacTxid    string = "3c545cfe89ec6614d083728211bca2d66a215027efdc4597d9970f473e7a4989"
	swAddr           string = "3PvVeEhWGPj5KT2ivdvWa3YoStMy3fQnch"
	swTransac        string = "0101000000000101f6901ba064d6d5a34815596b7f3114a937dd36c23fed9f275c2df197c566a22e0100000000ffffffff0240981e090000000017a914f3df5032df6c05aa38b8899e3186132052d3e461870e073b0500000000220020701a8d401c84fb13e6baf169d59684e17abd9fa216c8cc5b9fc63d622ff8c58d040047304402204da66f456ab24ebc2e4472aed14d70f0ce73b149129c1e1129437d978878076f02200d3ce14c78939e06f771ec1ec93dcda5a318a1e7c25f8bee5315db31f2c4b7cf014730440220752bd175310c47969324d903a984f672069e74153010bd274f357793126686a002206315f0bf7490e0bf4b5dad2dff05a3f7420a3293ccf45b5efc2bcd054fae1c68016952210375e00eb72e29da82b89367947f29ef34afb75e8654f6ea368e0acdfd92976b7c2103a1b26313f430c4b15bb1fdce663207659d8cac749a0e53d70eff01874496feff2103c96d495bfdd5ba4145e3e046fee45e84a8a48ad05bd8dbb395c011a32cf9f88053ae00000000"
	nonswTransacTxid string = "1bdb3a4623e76c002478b551ff7fa2f5179c8afc49ef9e7903590de5211ffca9"
	nonswAddr        string = "1CcX4NNf4AKTR1KTRrZx8xMVfJjtrxz7MB"
	nonswTransac     string = "0101000000013a29bba34fdee1b794d63e3346a1e7ca2f864929d5171f4062a18842193d812f010000006a473044022060d1ce94ea837d3ac1b9773d7841e8b00fa2712e595e1a1cea27e80ee17980b902203cee0ec7af6db855f23b0e853206a026a6acdbd90ecff3a824c86f93dd8b3710012103dfdf2596c250f5d91a0be8e6a3e4e8c24642473b6a71a2219953092b402ca688feffffff02a0860100000000001976a9147f610893b57173e11316643752880a0cdfd7908488ac9b801800000000001976a9146cb5ef5d2c7729615eaace7bed32557a9e6fdf0e88ac1c2e0900"
	// txid: 3fdf710d0aafefdfd00b53eb25cd678da09a04d45010043027de4e3fb74c9aa8
	inP2pkh     string = "46939cf5bc2716ce2e4e0d57d7a2d567d5a2f6b5db427a7c89da10e69e341b30040000008a4730440220396c494b2ccec124aebc9e69f447a4230a53f62cfda72ade61cedf366a31f2770220424ee1dab71674735f55d9eb7a6b1b60349792247fe10abf31ebf3a4e8e1ae210141047146f0e0fcb3139947cf0beb870fe251930ca10d4545793d31033e801b5219abf56c11a3cf3406ca590e4c14b0dab749d20862b3adc4709153c280c2a78be10cffffffff"
	inP2pkhAddr string = "17A16QmavnUfCW11DAApiJxp7ARnxN5pGX"
	// txid: bad242ddddc5002a541306d3e3187a6aa18ee3070f028b847307e1380b90427d
	inP2sh     string = "aa2dd682cd60c4ef095036a9037ce6ec7793870f93dbb4710b36983c553d4ae800000000fdfd00004730440220161602ab87a32c72eab1bcbce29d4d3029aeea83a1f4e1930223f80867885116022038d36fe6e9f371bb342bead9c2ad9b7d96822848647e0d7a4f80725f4cbf51a601483045022100a507b4a3b5bf82726dfd70ee766ec296dfc3b9055a74a711e9a9f6007da573ff0220779853bb9144c2e0c02207df8ce0b12195820f78c7c98dcf0fbe9f8f6ea49a69014c695221031de28534908adb89293f05b794a667e7d2e663b88bf01be735d2fe3912dbd70721034c1087c10f956e94f14e2d3575c0f852280f6f60f605ba6fcb9323a39fbd663521037dad2d1a20c976368ac6b9fb116ebd7e4ab19bfd17a732d169cd7166d2c82e2553aeffffffff"
	inP2shAddr string = "3QQB6AWxaga6wTs6Xwq8FYppgrGinGu15f"
	// txid: 581d30e2a73a2db683ac2f15d53590bd0cd72de52555c2722d9d6a78e9fea510
	outP2ms string = "90c9190000000000c9524104d81fd577272bbe73308c93009eec5dc9fc319fc1ee2e7066e17220a5d47a18314578be2faea34b9f1f8ca078f8621acd4bc22897b03daa422b9bf56646b342a24104ec3afff0b2b66e8152e9018fe3be3fc92b30bf886b3487a525997d00fd9da2d012dce5d5275854adc3106572a5d1e12d4211b228429f5a7b2f7ba92eb0475bb14104b49b496684b02855bc32f5daefa2e2e406db4418f3b86bca5195600951c7d918cdbe5e6d3736ec2abf2dd7610995c3086976b2c0c7b4e459d10b34a316d5a5e753ae"
	// txid: ccf165e23877c8afaf9d300634dcb050075fdb6d0edbf9bc3b481727bcc5bf97
	outP2sh     string = "be2918000000000017a91481c4151431ff4f039b49181e1a0443462f14858a87"
	outP2shAddr string = "3DX9zL5FKQ7JcNwbwZ2m3x4cenz1DVmzoB"
	// txid: 3f21ae8e5ddc3d505f404951f1c1a5ed0d0b0af54d66db6d6232e23016a4f09b
	outP2pkh     string = "73df0400000000001976a914d302bff7292de0b1857fbf3aacb7cd8657a00b6688ac"
	outP2pkhAddr string = "1LEisYLDvXymMEabACy6q6kvSEb5D7SmqA"
	// txid: 7ee98ab528ea69546c938b59137e80e327519ac37913b76ee51b9b0440a6a507
	outP2wpkh     string = "17430f000000000016001403adf0682bb65ba7c4d0500d0a99c4fc130d2ea9"
	outP2wpkhAddr string = "bc1qqwklq6ptked603xs2qxs4xwylsfs6t4fxvc4la"
	// txid: fe1bbb0b84874fce01d27b28d55770af78504fceb8229dcacfe6916d6029a16c
	outP2wsh     string = "db2c100000000000220020c24f8382442fb35a2044de6934b1b2576ade0255096aa3e466bf04ce715abd3b"
	outP2wshAddr string = "bc1qcf8c8qjy97e45gzyme5nfvdj2a4duqj4p9428erxhuzvuu26h5ask9kqnk"
	// txid: 1d0126217f770a25e4b6c36f1701568e740130132143144197319c714e2560e4
	inP2wshInP2sh      string = "6b760375e3e60a24de0759bb002f2842cc1edec164985e84c5cb108c03cd91f1060000001716001437d0cefbf8b71512f6048d42275ddc989aeece15ffffffff"
	inP2wshInP2shAddr  string = "3HyriztpMoPGuFQ1gqzE3hioHRwtjuWFht"
	outP2wshInP2sh     string = "a27300000000000017a9140c92be4106e9fae6b6e1655dd99317d9a4ce501587"
	outP2wshInP2shAddr string = "32qVpjucCAMQuwwQNaedS3sYbBY99kdjF8"
)

func loadBlocks(t *testing.T) {
	Params.NBlocks = 1
	Params.LoadBlockHash = true

	b, err := LoadBlocks(testblkfile)
	if err != nil {
		t.Fatal(err)
	}

	if b[0].BlockHash != blk1845blkhash {
		log.Println(b[0].BlockHash)
		t.Fatal("wrong block hash for block file 1845")
	}
	if b[0].Header.MerkleRoot != blk1845Mkroot {
		t.Fatal("wrong merkle root for block file 1845")
	}
}

func TestLoadBlocksNoAddressNoTxId(t *testing.T) {
	loadBlocks(t)
}

func TestLoadBlocksNoAddress(t *testing.T) {
	Params.LoadTxid = true

	loadBlocks(t)
}

func TestLoadBlocks(t *testing.T) {
	Params.LoadTxid = true
	Params.LoadAddr = true

	loadBlocks(t)
}

func loadTransac(t *testing.T, raw string, txid string) string {
	b := Block{}

	transac, err := hex.DecodeString(raw)
	if err != nil {
		t.Fatal(err)
	}
	err = b.ReadTransactions(transac)
	if err != nil {
		t.Fatal(err)
	}

	if Params.LoadTxid && b.Transac[0].Txid != txid {
		t.Fatal("txid does not match")
	}

	if Params.LoadRaw &&
		"01"+hex.EncodeToString(b.Transac[0].Raw) != raw {
		t.Fatal("raw data does not match")
	}

	return b.Transac[0].Outs[0].Address
}

func TestSegWitTransac(t *testing.T) {
	Params.LoadTxid = true
	if loadTransac(t, swTransac, swTransacTxid) != swAddr {
		t.Fatal("out[0] address doesn't match")
	}
}

func TestNonSegWitTransac(t *testing.T) {
	Params.LoadTxid = true
	if loadTransac(t, nonswTransac, nonswTransacTxid) != nonswAddr {
		t.Fatal("out[0] address doesn't match")
	}
}

func TestLoadRaw(t *testing.T) {
	Params.LoadRaw = true
	Params.LoadTxid = false

	loadTransac(t, nonswTransac, "")
}

func TestLoadRawSegWit(t *testing.T) {
	Params.LoadRaw = true
	Params.LoadTxid = false

	loadTransac(t, swTransac, "")
}

func TestSegWitTransacAddr(t *testing.T) {
	Params.LoadInAddr = true
	TestSegWitTransac(t)
}

func readInputs(t *testing.T, sig string) []Inputs {
	b := Block{
		TxCount: 1,
	}
	Params.LoadInAddr = true

	bp, err := hex.DecodeString(sig)
	if err != nil {
		t.Fatal(err)
	}
	inputs, err := b.ReadInputs(1, bytes.NewReader(bp))
	if err != nil {
		t.Fatal(err)
	}

	return inputs
}

func readOutputs(t *testing.T, sig string) []Outputs {
	b := Block{
		TxCount: 1,
	}
	Params.LoadAddr = true

	bp, err := hex.DecodeString(sig)
	if err != nil {
		t.Fatal(err)
	}
	outputs, err := b.ReadOutputs(1, bytes.NewReader(bp))
	if err != nil {
		t.Fatal(err)
	}

	return outputs
}

func TestReadInputsP2PKH(t *testing.T) {
	inputs := readInputs(t, inP2pkh)

	if inputs[0].Address[0] != inP2pkhAddr {
		t.Fatal(addressNoMatch)
	}
}

func TestReadInputsP2SH(t *testing.T) {
	inputs := readInputs(t, inP2sh)

	if inputs[0].Address[0] != inP2shAddr {
		t.Fatal(addressNoMatch)
	}
}

func TestReadOutputsP2MS(t *testing.T) {
	outputs := readOutputs(t, outP2ms)

	if outputs[0].LockType != "P2MS" {
		t.Fatal("wrong locktype")
	}
}

func TestReadOutputsP2SH(t *testing.T) {
	outputs := readOutputs(t, outP2sh)

	if outputs[0].Address != outP2shAddr {
		t.Fatal(addressNoMatch)
	}
}

func TestReadOutputsP2PKH(t *testing.T) {
	outputs := readOutputs(t, outP2pkh)

	if outputs[0].Address != outP2pkhAddr {
		t.Fatal(addressNoMatch)
	}
}

func TestReadOutputsP2WPKH(t *testing.T) {
	outputs := readOutputs(t, outP2wpkh)

	if outputs[0].Address != outP2wpkhAddr {
		t.Fatal(addressNoMatch)
	}
}

func TestReadOutputsP2WSH(t *testing.T) {
	outputs := readOutputs(t, outP2wsh)

	if outputs[0].Address != outP2wshAddr {
		t.Fatal(addressNoMatch)
	}
}

func TestP2WSHInP2SH(t *testing.T) {
	inputs := readInputs(t, inP2wshInP2sh)
	outputs := readOutputs(t, outP2wshInP2sh)

	if inputs[0].Address[0] != inP2wshInP2shAddr {
		t.Fatalf("input %s: %s vs %s",
			addressNoMatch, inputs[0].Address[0], inP2wshInP2shAddr)
	}
	if outputs[0].Address != outP2wshInP2shAddr {
		t.Fatalf("output %s: %s vs %s",
			addressNoMatch, outputs[0].Address, outP2wshInP2shAddr)
	}
}
