image: golang:1.14-buster

stages:
  - build
  - test
  - release

before_script:
  - export GOPATH=${CI_PROJECT_DIR}/.cache
  - export GOARCH=amd64
  - export OSS="linux freebsd netbsd darwin"
  - export BASEURL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}"
  - export BTCBLOCKSHOME=samples
  - export BTCBLOCKINDEX=${BTCBLOCKSHOME}/testindex
  - export BTCCHAINSTATE=${BTCBLOCKSHOME}/testchainstate
  - export BTCADDRDB=${BTCBLOCKSHOME}/testaddrdb

# I know best practices say not to use cache to transport
# artifacts from one stage to another, but artifacts are not
# supported by gitlab-runner exec and I like to test my
# builds locally before pushing.
# Ideas taken from https://blog.extrawurst.org/general/2018/12/05/go-and-gitlab-ci.html
# and https://www.modio.se/local-debugging-for-gitlab-ci-runners.html

build:
  stage: build
  cache: &build_cache
    key: build
    paths:
      - .cache
      - goxplorer
  script:
    - mkdir -p .cache
    - make
  only:
    changes:
      - "**/*.go"
      - Makefile

unit_tests:
  stage: test
  cache: *build_cache
  script:
    - apt-get update -qq && apt-get -y -qq install jq xxd
    - make test
    - /bin/sh runtest.sh
  dependencies:
    - build

code_coverage:
  stage: test
  script:
    - make coverage

release:
  stage: release
  script:
    - for os in ${OSS}; do GOOS=$os GOARCH=${GOARCH} make cross; done
    - apt-get update -qq && apt-get -qq -y install curl jq
    - /bin/sh mkrel.sh goxplorer
  artifacts:
    paths:
      - goxplorer_*
  only:
    - tags

pages:
  image: debian:buster-slim
  stage: release
  script:
    - apt-get update -qq && apt-get -qq -y install curl jq
    - /bin/sh pages/mkpages.sh
  artifacts:
    paths:
      - public
  when: manual
  only:
    - master
