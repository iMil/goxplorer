/*
 * Goxplorer, a blockchain file explorer
 * Copyright (C) 2019 Emile `iMil" Heitor
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package callbacks

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"

	"github.com/syndtr/goleveldb/leveldb"
	"gitlab.com/iMil/goxplorer/blockchain"
)

var Callbacks = map[string]interface{}{
	"tdemo":       TDemo,
	"countB":      CountB,
	"countT":      CountT,
	"csv":         PrintCSV,
	"out":         PrintOutAddr,
	"bruteforce":  Bruteforce,
	"mkdballaddr": MkdbAllAddr,
}

func LsCallbacks() {
	for c := range Callbacks {
		fmt.Println(c)
	}
}

var count int = 1

// tDemo is a demo transaction callback that will print txid,
// in and out addresses
func TDemo(b blockchain.Block, t blockchain.TransacData) {
	if blockchain.Params.LoadTxid {
		sw := ""
		if t.SegWit {
			sw = "[segwit]"
		}
		fmt.Printf("txid: %v %s\n", t.Txid, sw)
	}
	for _, ins := range t.Ins {
		if len(ins.Address) > 0 { // coinbase has no inputs
			fmt.Printf("in addr: %v\n", ins.Address[0])
		}
	}
	for _, outs := range t.Outs {
		fmt.Printf("out addr: %v (%s)\n", outs.Address, outs.LockType)
		if outs.LockType == "P2PK" {
			k, _ := json.MarshalIndent(outs, "", "  ")
			fmt.Println(string(k))
		}
	}
}

// CountB is a demo block callback that will print the current
// processed blockchain
func CountB(b blockchain.Block) {
	fmt.Printf("New block processed %d\n", count)
	count++
}

// CountT is a demo transaction callback that will print the current
// processed blockchain
func CountT(b blockchain.Block, t blockchain.TransacData) {
	fmt.Printf("\r%d transactions", count)
	count++
}

// TypeLoop parses the struct passed as s and builds a CSV row or prints struct
// names if the w(ant) parameter is nil
func typeLoop(w []string, parent string, s interface{}) []string {
	v := reflect.ValueOf(s)

	var row []string

	plabel := func(p string, label string) string {
		if len(p) > 0 {
			return p + "." + label
		}
		return label
	}

	// loop through transaction struct fields
	for i := 0; i < v.Type().NumField(); i++ {
		field := v.Type().Field(i)
		tj := field.Tag.Get("json")
		// we hit the inputs field, an array of input structs
		var input []string
		if tj == "inputs" {
			for _, ins := range (s.(blockchain.TransacData)).Ins {
				input = append(input, typeLoop(w, "inputs", ins)...)
				if w == nil {
					break
				}
			}
			if len(input) > 0 {
				row = append(row, "\""+strings.Join(input, ",")+"\"")
			}
		}
		// we hit the outputs field, an array of output structs
		if tj == "outputs" {
			var output []string
			for _, outs := range (s.(blockchain.TransacData)).Outs {
				output = append(output, typeLoop(w, "outputs", outs)...)
				if w == nil {
					break
				}
			}
			if len(output) > 0 {
				row = append(row, "\""+strings.Join(output, ",")+"\"")
			}
		}
		for _, p := range w {
			if strings.HasPrefix(plabel(parent, tj), p) {
				val := fmt.Sprintf("%v", v.Field(i))
				if len(val) == 0 {
					val = "null"
				}
				row = append(row, val)
			}
		}
		if w == nil {
			f := strings.Split(tj, ",")
			fmt.Println(plabel(parent, f[0]))
		}
	}
	return row
}

// PrintCSV outputs fields passed in parameter to tcparam as a bssic CSV format
func PrintCSV(b blockchain.Block, t blockchain.TransacData) {
	if len(blockchain.Params.TFuncParam) == 0 {
		return
	}

	if blockchain.Params.TFuncParam == "lsfields" {
		typeLoop(nil, "", t)
		os.Exit(0)
	}

	want := strings.Split(blockchain.Params.TFuncParam, ",")
	row := typeLoop(want, "", t)
	fmt.Println(strings.Join(row, ", "))
}

// PrintOutAddr prints all transactions output addresses
func PrintOutAddr(b blockchain.Block, t blockchain.TransacData) {
	for _, o := range t.Outs {
		fmt.Println(o.Address)
	}
}

func MkdbAllAddr(allBlocks []blockchain.Block) {
	if len(blockchain.Params.ABFuncParam) == 0 {
		log.Fatal("missing database name as parameter")
	}
	db, err := leveldb.OpenFile(blockchain.Params.ABFuncParam, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	batch := new(leveldb.Batch)
	i := 0
	for _, b := range allBlocks {
		for _, t := range b.Transac {
			for _, out := range t.Outs {
				if i%100000 == 0 {
					fmt.Printf("records: %d\n", i)
				}
				h := make([]byte, 8)
				binary.LittleEndian.PutUint64(h, uint64(t.Ins[0].Height))
				a := make([]byte, 8)
				binary.LittleEndian.PutUint64(a, out.Value)
				k := append([]byte(out.Address), t.Txid...)
				v := append(h, a...)
				batch.Put(k, v)
				i++
			}
		}
	}
	db.Write(batch, nil)
}
