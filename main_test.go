package main

import (
	"testing"

	"gitlab.com/iMil/goxplorer/blockchain"
)

func btoflag(bf []bool, n int) {
	for i, _ := range bf {
		if n&(1<<uint(i)) != 0 {
			bf[i] = true
		}
	}
}

// TestParams tests evry boolean parameter combination
func TestParams(t *testing.T) {
	for i := 0; i < 32; i++ {
		bf := []bool{false, false, false, false, false}
		btoflag(bf, i)
		blockchain.Params = blockchain.SParams{
			NBlocks:       1,
			LoadTxid:      bf[0],
			LoadBlockHash: bf[1],
			LoadAddr:      bf[2],
			LoadInAddr:    bf[3],
			LoadRaw:       bf[4],
		}
		b, err := blockchain.LoadBlocks(testblkfile)

		if len(b) == 0 {
			t.Errorf("Empty result with params: %v", blockchain.Params)
		}
		if err != nil {
			t.Errorf("%v, params: %v", err, blockchain.Params)
		}
	}
}

func TestRunMain(t *testing.T) {
	*blkfilenum = 1845
	main()
}
